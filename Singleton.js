let instance = null;

class Car {
    constructor(doors, engine, color) {
        if (!instance) {
            this.doors = doors;
            this.engine = engine;
            this.color = color;
            instance = this;
        }
        else {
            return instance;
        }
    }
}

// class SUV extends Car {
//     constructor(doors, engine, color) {
//         super(doors, engine, color);
//         this.wheels = 4;
//     }
// }

const civic = new Car("4", "V9", "ALpRed");
const rsp = new Car("4", "V12", "Red");
// const rsp = new SUV("9", "V6", "yellow");
console.log(civic);
console.log(rsp);