class Car {
    constructor(doors, engine, color) {
        this.doors = doors;
        this.engine = engine;
        this.color = color;
    }
}

class carFactory {
    createCar(type) {
        switch (type) {
            case 'civic':
                return new Car(4, 'v6', 'red');
            case 'honda':
                return new Car(5, 'v8', 'yellow');
            default:
                return new Car('*', 'v*', 'Whater')
        }
    }
}

const exe = new carFactory;

console.log(exe.createCar('honda'));
console.log(exe.createCar('civic'));
console.log(exe.createCar('maruti'))
