class Car {
    constructor(doors, engine, color) {
        this.doors = doors;
        this.engine = engine;
        this.color = color;
    }
}

class CarFactory {
    createCar(type) {
        switch (type) {
            case 'civic':
                return new Car(4, 'v6', 'red');
            case 'honda':
                return new Car(5, 'v8', 'yellow');
            default:
                return new Car('*', 'v*', 'Whater')
        }
    }
}

class SUV {
    constructor(doors, engine, color) {
        this.doors = doors;
        this.engine = engine;
        this.color = color;
    }
}
class SuvFactory {
    createCar(type) {
        switch (type) {
            case 'cx5':
                return new Car(4, 'v6', 'red');
            case 'honda':
                return new Car(5, 'v8', 'yellow');
            default:
                return new Car('*', 'v*', 'Whater')
        }
    }
}

const carFactory = new CarFactory();
const suvFactory = new SuvFactory();

const autoManufacturer = (type, model) => {
    switch (type) {
        case 'car':
            return carFactory.createCar(model)
        case 'suv':
            return suvFactory.createCar(model)
    }
}

const cx5 = autoManufacturer('suv', 'cx5')
console.log(cx5);